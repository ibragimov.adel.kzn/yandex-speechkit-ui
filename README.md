### How to run app?

1. npm install
2. npm run build
3. npm start

### Required env variables (.env)

```
OBJECT_STORAGE_NAME = [название бакета]
OBJECT_STORAGE_ACCESS_KEY = [id статического ключа]
OBJECT_STORAGE_SECRET_KEY = [значение статического ключа]
```

1. Нажать "Выбрать файл" - выбрать mp3 файл из файловой системы
2. Ввести API ключ сервисного аккаунта в поле "API ключ"
3. Нажать "Запустить"
