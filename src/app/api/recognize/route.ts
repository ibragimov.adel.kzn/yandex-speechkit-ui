import {
  PutObjectCommand,
  S3Client,
  GetObjectCommand,
} from '@aws-sdk/client-s3';

const s3Client = new S3Client({
  region: 'ru-central1',
  endpoint: 'https://storage.yandexcloud.net',
  credentials: {
    accessKeyId: process.env.OBJECT_STORAGE_ACCESS_KEY!,
    secretAccessKey: process.env.OBJECT_STORAGE_SECRET_KEY!,
  },
});

export async function POST(request: Request) {
  const formData = await request.formData();
  const apiKey = formData.get('apiKey') as string;
  const audio = formData.get('audio') as string;
  let audioEncoding;

  if (audio.includes('.mp3')) {
    audioEncoding = 'MP3';
  } else if (audio.includes('.ogg')) {
    audioEncoding = 'OGG_OPUS';
  } else {
    return Response.json(
      {
        message: 'Incorrect video format',
      },
      {
        status: 400,
      },
    );
  }

  if (!apiKey || !audio) {
    return Response.json(
      {
        message: 'API Key or video not provided',
      },
      {
        status: 400,
      },
    );
  }

  try {
    const a = await s3Client.send(
      new GetObjectCommand({
        Bucket: process.env.OBJECT_STORAGE_NAME,
        Key: audio,
      }),
    );

    const res = await fetch(
      'https://transcribe.api.cloud.yandex.net/speech/stt/v2/longRunningRecognize',
      {
        method: 'POST',
        headers: {
          Authorization: `Api-Key ${apiKey}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          audio: {
            uri: `https://storage.yandexcloud.net/${process.env.OBJECT_STORAGE_NAME}/${audio}`,
          },
          config: {
            specification: {
              audioEncoding,
              languageCode: 'ru-RU',
              model: 'general',
              profanityFilter: false,
              literature_text: true,
              sampleRateHertz: 48 * 1000,
              rawResults: true,
            },
          },
        }),
      },
    );

    const data = await res.json();
    return Response.json(data);
  } catch (e) {
    return Response.json(e, {
      status: 500,
    });
  }
}

export async function PUT(request: Request) {
  const formData = await request.formData();
  const audio = formData.get('audio') as File;

  if (!audio) {
    return Response.json(
      {
        message: 'Audio not provided',
      },
      {
        status: 400,
      },
    );
  }

  try {
    const arrayBuffer = await audio.arrayBuffer();
    const unit8Array = new Uint8Array(arrayBuffer);

    const res = await s3Client.send(
      new PutObjectCommand({
        Bucket: process.env.OBJECT_STORAGE_NAME,
        Key: audio.name,
        Body: unit8Array,
      }),
    );

    return Response.json(res);
  } catch (e) {
    return Response.json(e, {
      status: 500,
    });
  }
}

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const operationId = searchParams.get('operationId') as string;
  const apiKey = searchParams.get('apiKey') as string;

  try {
    const response = await fetch(
      `https://operation.api.cloud.yandex.net/operations/${operationId}`,
      {
        headers: {
          Authorization: `Api-Key ${apiKey}`,
          'Content-Type': 'application/json',
        },
      },
    );
    const data = await response.json();
    return Response.json(data);
  } catch (e) {
    return Response.json(e, {
      status: 500,
    });
  }
}
