import Form from '@/components/Form';

export default function Home() {
  return (
    <main className="min-h-screen flex items-center justify-center bg-gradient-radial from-orange-100 to-orange-800">
      <Form />
    </main>
  );
}
