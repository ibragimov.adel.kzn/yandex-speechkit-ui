'use client';

import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import Loader from '@/components/Loader';

const Form = () => {
  const [apiKey, setApiKey] = useState('');
  const [loading, setLoading] = useState(false);
  const [audio, setAudio] = useState<File>();
  const [result, setResult] = useState('');
  const [operationId, setOperationId] = useState('');
  const [error, setError] = useState<any>('');

  const statusIntervalFunction = useCallback(() => {
    if (!operationId) {
      return;
    }

    updateStatus();
  }, [operationId]);

  const onSubmit = async () => {
    setLoading(true);

    setResult('');
    setError('');
    await uploadFile();
    await recognize();
  };

  const updateStatus = async () => {
    const formData = new FormData();
    formData.append('operationId', operationId);

    try {
      const response = await fetch(
        `/api/recognize?operationId=${operationId}&apiKey=${apiKey}`,
        {
          method: 'GET',
        },
      );

      if (!response.ok) {
        const errorText = await response.text();
        setLoading(false);
        return setError(JSON.parse(errorText).message);
      }

      const data = await response.json();

      if (data.done) {
        setOperationId('');
        setLoading(false);

        if (data.error) {
          return setError(data.error.message);
        }

        if (data.response.chunks) {
          setResult(
            data.response.chunks
              .map((chunk: any) => chunk.alternatives[0].text)
              .join('\n'),
          );
        }
      }
    } catch (e) {
      setError(e);
      setLoading(false);
    }
  };

  const recognize = async () => {
    if (!audio) {
      return;
    }

    const formData = new FormData();
    formData.append('apiKey', apiKey);
    formData.append('audio', audio.name);

    try {
      const response = await fetch(`/api/recognize`, {
        method: 'POST',
        body: formData,
      });

      if (!response.ok) {
        const err = JSON.parse(await response.text());
        setLoading(false);
        return setError(err.message ? err.message : err);
      }

      const data = await response.json();
      setOperationId(data.id);
    } catch (e) {
      setError(e);
      setLoading(false);
    } finally {
    }
  };

  const uploadFile = async () => {
    if (!audio) {
      return;
    }

    const formData = new FormData();
    formData.append('audio', audio);

    try {
      const response = await fetch(`/api/recognize`, {
        method: 'PUT',
        body: formData,
      });

      if (!response.ok) {
        const errorText = await response.text();
        setLoading(false);
        return setError(JSON.parse(errorText));
      }
    } catch (e) {
      setError(e);
      setLoading(false);
    } finally {
    }
  };

  const fileInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;

    if (files) {
      const audio = files[0];
      setAudio(audio);
    }
  };

  useEffect(() => {
    const interval = setInterval(statusIntervalFunction, 5 * 1000);

    return () => clearInterval(interval);
  }, [statusIntervalFunction]);

  return (
    <div className="rounded-lg p-[20px] backdrop-blur-md bg-white/30 w-[480px] flex flex-col gap-[50px] items-center">
      <div className="w-full h-[480px] flex flex-col gap-[20px]">
        <label className="relative">
          <input
            onChange={fileInputChange}
            className="invisible absolute"
            type="file"
          />
          <span className="underline cursor-pointer">Выбрать файл</span>
          {audio && (
            <span className="ml-[5px] text-black/50">({audio.name})</span>
          )}
        </label>

        {loading && (
          <div className="flex-1 flex items-center justify-center">
            <Loader />
          </div>
        )}
        {!loading && (
          <div className="bg-white rounded-[5px] flex-1 p-[20px] overflow-y-auto">
            {result.split('\n').map((item) => (
              <p key={item}>{item}</p>
            ))}
          </div>
        )}
        {error && (
          <pre className="p-[20px] bg-red-400 rounded-[5px]">
            {typeof error === 'string' ? error : JSON.stringify(error, null, 2)}
          </pre>
        )}
      </div>

      <div className="flex flex-col gap-[20px] w-full">
        <input
          className="w-full rounded-[5px] p-[10px] outline-none border-[1px] transition-all hover:border-purple-400 focus:border-purple-800"
          type="text"
          placeholder="API Ключ"
          value={apiKey}
          onChange={(e) => setApiKey(e.target.value)}
        />

        <button
          className="bg-purple-700 rounded-[5px] p-[10px] w-full text-white uppercase font-[600] transition-all hover:bg-purple-800 disabled:bg-purple-700/30 disabled:cursor-not-allowed"
          onClick={onSubmit}
          disabled={!apiKey.length || !audio}
        >
          Запустить
        </button>
      </div>
    </div>
  );
};

export default Form;
