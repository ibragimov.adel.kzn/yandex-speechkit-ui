const Loader = () => {
  return (
    <div className="animate-spin w-[60px] h-[60px] rounded-full border-[8px] border-blue-700 border-l-transparent"></div>
  );
};

export default Loader;
