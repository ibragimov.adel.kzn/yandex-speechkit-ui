'use client';

import Image from 'next/image';
import { useAudioRecorder } from 'react-audio-voice-recorder';
import cn from 'classnames';
import { FC, useEffect } from 'react';

interface Props {
  callback?: (audio: Blob) => void;
}

const RecordButton: FC<Props> = ({ callback }) => {
  const { startRecording, stopRecording, isRecording, recordingBlob } =
    useAudioRecorder();

  const toggleRecord = () => {
    if (isRecording) {
      stopRecording();
    } else {
      startRecording();
    }
  };

  useEffect(() => {
    if (!recordingBlob) {
      return;
    }

    callback && callback(recordingBlob);
  }, [recordingBlob, callback]);

  return (
    <button
      className={cn(
        'w-[120px] h-[120px] bg-purple-800 rounded-full shadow-lg relative',
      )}
      onClick={toggleRecord}
    >
      <div
        className={cn(
          'absolute t-0 l-0 w-full h-full bg-purple-800 rounded-full',
          {
            'animate-ping': isRecording,
          },
        )}
      ></div>
      <Image
        className="z-50 relative"
        src="/mic.svg"
        alt=""
        width={256}
        height={256}
      />
    </button>
  );
};

export default RecordButton;
